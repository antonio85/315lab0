#Antonio Silva Paucar
#Lab 0 CIS 315

from sys import argv

def main(argv):

    fileName = argv[1]
    size = 0
    with open(fileName, 'r') as fob:

        for line in fob:
            line = line.strip().split()
            if len(line) == 2:
                sum = int(line[0].strip()) + int(line[1].strip())
                mul = int(line[0].strip()) * int(line[1].strip())
                print "{} {}".format(sum, mul)





if __name__ == "__main__":
    main(argv)
